function hideResults(){
	var displayResults = document.getElementById("results");
	displayResults.style.display="none";
}

function play(){

	var betStart = +document.getElementById("betStartInput").value;
	var diceCounter = 1;
	var highBet = 0;
	var diceRollHigh = 0;
	var playAgain = 0;


	for (bet = betStart; bet > 0; diceCounter++) {
		var diceRoll=(Math.floor(Math.random() * 6) +1) + (Math.floor(Math.random() * 6) +1);
			
		if (diceRoll == 7){
			bet = bet + 4;
			} else {
			bet = bet - 1;
		}

		if (highBet < bet){
			highBet = bet;
			diceRollHigh = diceCounter;
		}

		if (bet <= 0) {
			var playAgain = 1;
		}

		if (highBet < betStart) {
			highBet = betStart;
		}
	}

	if (playAgain = 1){
		var resetButton = document.getElementById("playText");
		var resultbetstart = document.getElementById("printbetStart");
		var resultdiceCounter = document.getElementById("printdiceCounter");
		var resulthighBet = document.getElementById("printhighBet");
		var resultdiceRollHigh = document.getElementById("printdiceRollHigh");
		var correctbetStart = betStart.toFixed(2);
		var correcthighBet = highBet.toFixed(2);

		resetButton.innerHTML = "Play Again";
		resultbetstart.innerHTML = "$" + correctbetStart;
		resultdiceCounter.innerHTML = diceCounter;
		resulthighBet.innerHTML = "$" + correcthighBet;
		resultdiceRollHigh.innerHTML = diceRollHigh;

		var displayResults = document.getElementById("results");
		displayResults.style.display="inline";
	}

}