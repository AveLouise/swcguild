function checkValues(){
	var checkName = document.forms["contactUs"]["Name"].value;
	var checkEmail = document.forms["contactUs"]["Email"].value;
	var checkPhone = document.forms["contactUs"]["Phone"].value;
	var checkReason = document.forms["contactUs"]["Reason"].value;
	var checkaddInfo = document.forms["contactUs"]["addInfo"].value;
	var checkDay = document.forms["contactUs"].elements["Day[]"];

	if (checkName == null || checkName == "") {
		alert("Please enter your name. We need to know who you are!");
		return false;
	}

	if ((checkEmail == null || checkEmail == "") && (checkPhone == null || checkPhone == "")){
		alert("Please enter one form of contact information, either a phone number or an email address. We want to be able to get back to you!");
		return false;
	}
	
	if (checkReason == "Other" && (checkaddInfo == null || checkaddInfo == "")){
		alert("Since you have selected 'Other' under 'Reason for Inquiry,' please tell us why you are inquiring in 'Additional Information' so we can best address your needs.");
		return false;
	}

	for (var i=0, len=checkDay.length, checkChecked = 0; i<len; i++){
		if (checkDay[i].checked == true){
			checkChecked = checkChecked + 1;
		}
	}

	if (checkChecked == 0){
		alert("Please select a day which would be best to contact you. (Monday-Friday)");
		return false;
	}

}